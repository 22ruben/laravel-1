<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='{{asset("css/estilos.css")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset("css/normalize.css")}}' rel='stylesheet' type='text/css'>
    <title>CRUD CON LARAVEL</title>
</head>
<body
    <main>
    <header>
    <h1>CRUD CON LARAVEL</h1>
    </header>
        <h3>Es obligatorio completar todos los campos</h3>
        <form action="enviar.php" method="post">

            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" placeholder="Nombre del producto" maxlength="20" required autofocus>

            <br><br><label for="cantidad">Cantidad</label>
            <input type="number" name="cantidad" id="cantidad" placeholder="Nombre del producto" required autofocus>

            <br><br><button type="submit" id="enviar" >ENVIAR</button>
        </form>

</main>

</body>
</html>
