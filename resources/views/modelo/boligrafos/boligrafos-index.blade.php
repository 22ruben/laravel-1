@extends('layouts.plantilla')


@section('content')
    @foreach ($boligrafo as $s)
        <a href="{{ route('boligrafo-show', $s->id) }}"><p>ID: {{ $s->id }}</p></a>
        <p>NOMBRE: {{ $s->nombre }}</p>
        <p>CANTIDAD: {{ $s->cantidad }}</p>
        <a href="{{ route('boligrafo-edit', $s->id) }}">Editar Boligrafo</a>
        <a href="{{ route('boligrafo-destroy', $s->id) }}">Eliminar Boligrafo</a>
        
        <br><br><br>
    @endforeach
@endsection
