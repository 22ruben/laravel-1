<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class Papeleria extends Model

{

protected $table = 'boligrafo';
protected $primaryKey = 'boligrafo_id';

protected $attributes = [
    'agotado' => false,
];

protected $fillable = ['name', 'description'];

}

$boligrafo1 = new Producto1;
$boligrafo1->nombre = 'Bic';
$boligrafo1->descripcion = 'Azul';
$boligrafo1->save();