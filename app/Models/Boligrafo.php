<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boligrafo extends Model
{
    use HasFactory;
    protected $table = 'boligrafo';

    protected $fillable = [
        'nombre', 'cantidad'
    ];
}
