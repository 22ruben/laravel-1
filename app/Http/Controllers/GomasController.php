<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GomasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $goma = DB::table('gomas')->get();
        return view('modelo.gomas.gomas-index', compact('goma'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.gomas.goma-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('gomas')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([GomasController::class, 'index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $goma = Goma::where('id', '=', $id)->first();
        return view('modelo.gomas.goma-show', compact('goma'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $goma = DB::table('gomas')->where('id', '=', $id)->first();
        return view('modelo.gomas.gomas-edit', compact('goma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('gomas')->where('id', '=', $id)->update(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([GomasController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('gomas')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
