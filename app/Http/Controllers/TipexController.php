<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TipexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipex = DB::table('tipex')->get();
        return view('modelo.tipex.tipex-index', compact('tipex'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.tipex.tipex-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('tipex')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([TipexController::class, 'index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipex = Tipex::where('id', '=', $id)->first();
        return view('modelo.tipex.tipex-show', compact('tipex'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipex = DB::table('tipex')->where('id', '=', $id)->first();
        return view('modelo.tipex.tipex-edit', compact('tipex'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('tipex')->where('id', '=', $id)->update(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([TipexController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tipex')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
