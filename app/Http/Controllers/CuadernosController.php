<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CuadernosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuaderno = DB::table('cuadernos')->get();
        return view('modelo.cuadernos.cuadernos-index', compact('cuaderno'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.cuadernos.cuaderno-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('cuadernos')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([CuadernosController::class, 'index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cuaderno = Cuaderno::where('id', '=', $id)->first();
        return view('modelo.cuadernos.cuaderno-show', compact('cuaderno'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuaderno = DB::table('cuadernos')->where('id', '=', $id)->first();
        return view('modelo.cuadernos.cuaderno-edit', compact('cuaderno'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('cuadernos')->where('id', '=', $id)->update(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([CuadernosController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('cuadernos')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
