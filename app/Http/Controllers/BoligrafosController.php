<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BoligrafosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boligrafo = DB::table('boligrafos')->get();
        return view('modelo.boligrafos.boligrafos-index', compact('boligrafo'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.boligrafos.boligrafo-create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('boligrafos')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([BoligrafosController::class, 'index']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $boligrafo = Boligrafo::where('id', '=', $id)->first();
        return view('modelo.boligrafos.boligrafo-show', compact('boligrafo'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boligrafo = DB::table('boligrafos')->where('id', '=', $id)->first();
        return view('modelo.boligrafos.boligrafo-edit', compact('boligrafo'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('boligrafos')->where('id', '=', $id)->update(
            array(
                "nombre" => $request->input('nombre'),
                "cantidad" => $request->input('cantidad'),
            )
        );
        return redirect()->action([BoligrafosController::class, 'index']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('boligrafos')->where('id', '=', $id)->delete();
        return redirect()->back();

    }
}
